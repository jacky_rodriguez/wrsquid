module Wrsquid
  class Configuration

    attr_reader :server_type,
                  :username,
                  :api_key,
                  :channel_id,
                  :channel_url,
                  :merchant_id
 
    def initialize(conf)
      @server_type = conf["server_type"]
      @username = conf["username"]
      @api_key = conf["api_key"]
      @channel_id = conf["channel_id"]
      @channel_url = conf["channel_url"]
      @merchant_id = conf["merchant_id"]
    end


    def self.headers(content_type=nil)
      type = Wrsquid.filterize_content('application/json', content_type)
      data = Wrsquid.load_config
      {
        "username" => data["username"],
        "api_key" => data["api_key"],
        "server_type" => data["server_type"],
        "Content-Type" => type
      }
    end

    # TEMPORARY - 
    def get_token
      # url for DEV USE 
      url = "https://api.acommercedev.com/identity/token"

      data_credentials = { auth: {apiKeyCredentials:{username: @username, apiKey: @api_key}} }

      results = HTTParty.post(url,
      { 
        :body => data_credentials.to_json,
        :headers => {"Content-Type" => "application/json"}
      })

      results.parsed_response["token"]["token_id"]
    end

  end
end




