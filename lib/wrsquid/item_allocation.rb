module Wrsquid
  class Item_allocation

    def self.inventory
      url = Wrsquid.channel_url(["allocation", "merchant", Wrsquid.configuration.merchant_id]) #("allocation/merchant/1175")
      HTTParty.get(url, :headers => Wrsquid::Configuration.headers).body
    end
  end
end
