module Wrsquid
  class Merchant

    def self.authenticate_merchant
      url = Wrsquid.channel_url("merchants")
      datas = HTTParty.get(url, :headers => Wrsquid::Configuration.headers).body
      JSON.parse(datas)["data"]
    end

  end
end
