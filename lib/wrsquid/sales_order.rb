module Wrsquid
  class Sales_order

    attr_reader :data
    def initialize(attributes={})
      @data = attributes[:data]
    end

    # WIP
    def create(data, order_id) 
      url = Wrsquid.channel_url(["order", order_id])
      HTTParty.put(url,
      { 
        :body => data.to_json,
        :headers => Wrsquid::Configuration.headers
      })
    end

    # TEMPORARY DIRECT SEND to SQUID API (dev)
    def self.create_so_to_squid(data, order_id)
      token = Wrsquid.configuration.get_token
      url = "https://fulfillment.api.acommercedev.com/channel/shopifydummyclient/order/#{order_id}"
      if token
        HTTParty.put(url,
        { 
          :body => data.to_json,
          :headers =>  {"X-Subject-Token" => token,"Content-Type" => "application/json"}
        })
      end
    end

    def self.retrieve(order_id)
      url = Wrsquid.channel_url(["order", order_id])
      HTTParty.get(url, :headers => Wrsquid::Configuration.headers).body
    end

    def self.retrieve_status_update(iso_date, status) 
      # COMPLETED , NEW, IN_PROGRESS, CANCELLED
      date = Wrsquid.filterize_content((DateTime.now - 1).iso8601, iso_date)

      url = Wrsquid.channel_url("sales-order-status?since=#{date}&orderStatus=#{status}")
      HTTParty.get(url, :headers => Wrsquid::Configuration.headers).body
    end

  end
end
