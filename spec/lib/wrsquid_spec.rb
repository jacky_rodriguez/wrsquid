require 'spec_helper'


describe Wrsquid do

  describe "#filterize_content" do
    it "should return string or array" do
      filtered_content = Wrsquid.filterize_content([], "url")
      expect(filtered_content).to eq "url"
    end
  end

end