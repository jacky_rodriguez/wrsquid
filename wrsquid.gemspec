# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'wrsquid/version'

Gem::Specification.new do |spec|
  spec.name          = "wrsquid"
  spec.version       = Wrsquid::VERSION
  spec.authors       = ["Jacky Rodriguez"]
  spec.email         = ["jackelyn.rodriguez@acommerce.asia"]

  spec.summary       = %q{Gem that connects to acommerce squid}
  spec.description   = %q{Gem that connects to acommerce squid api}
  # spec.homepage      = "TODO: Put your gem's website or public repo URL here. | BITBUCKET "
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "TODO: Set to 'http://mygemserver.com'"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject do |f|
    f.match(%r{^(test|spec|features)/})
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.13"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "rspec"

  spec.add_development_dependency "minitest"
  spec.add_development_dependency "vcr"
  spec.add_development_dependency "webmock"
  # spec.add_development_dependency "httparty"
  spec.add_dependency "httparty", "~> 0.14.0"
  # spec.add_dependency "json"
end

